console.log("lala");

jQuery(document).ready(function ($) {
	// ---------------------------------------------------

	// Technologies img tooltips init
	$(".tech-list img").each(function () {
		var alt = $(this).attr("alt");
		$(this).attr("title", alt);
		$(this).simpletooltip({
			position: "bottom",
			color: "#FFFFFF",
			background_color: "rgba(0,0,0,0.75)",
			border_width: 4,
		});
	});

	$(function () {
		var options = $(".tags").html();
		var temp = options.split(", "); // first split string and convert it to array
		var str = "";
		$.each(temp, function (i, v) {
			// loop through array
			str += "<span>" + v + "</span>"; // create html string and store it in str variable
		});
		$(".tags").empty().append(str);
	});

	// ---------------------------------------------------
});
